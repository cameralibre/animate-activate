const easeInOutQuint = 'cubic-bezier(0.83, 0, 0.17, 1)'
const easeOutBack = 'cubic-bezier(0.34, 1.56, 0.64, 1)'
const easeInQuint = 'cubic-bezier(0.64, 0, 0.78, 0)'
const easeInExpo = 'cubic-bezier(0.7, 0, 0.84, 0)'

const defaultTimings = {
  fill: 'both',
  duration: 6800,
  iterations: 1
}

const keyframes = {
  iSlide: [
    // animate
    { transform: 'translateX(0px)', opacity: 1, easing: easeInOutQuint, offset: 0.103 },
    { opacity: 0, offset: 0.123 },
    { transform: 'translateX(6.2px)', opacity: 1, offset: 0.143 },
    // activate
    { transform: 'translateX(6.2px)', opacity: 1, easing: easeInOutQuint, offset: 0.270 },
    { opacity: 0, offset: 0.290 },
    { transform: 'translateX(0px)', opacity: 1, offset: 0.310 },
    // animate
    { transform: 'translateX(0px)', opacity: 1, easing: easeInOutQuint, offset: 0.437 },
    { opacity: 0, offset: 0.457 },
    { transform: 'translateX(6.2px)', opacity: 1, offset: 0.477 },
    // activate
    { transform: 'translateX(6.2px)', opacity: 1, easing: easeInOutQuint, offset: 0.603 },
    // animate
    { transform: 'translateX(6.2px)', opacity: 1, offset: 1 }
  ],
  iSlideBlur: [
    // animate
    { opacity: 0, offset: 0 },
    { transform: 'translateX(0px)', opacity: 0, easing: easeInOutQuint, offset: 0.103 },
    { opacity: 1, offset: 0.123 },
    { transform: 'translateX(6.2px)', opacity: 0, offset: 0.143 },
    // activate
    { transform: 'translateX(6.2px)', opacity: 0, easing: easeInOutQuint, offset: 0.270 },
    { opacity: 1, offset: 0.290 },
    { transform: 'translateX(0px)', opacity: 0, offset: 0.310 },
    // animate
    { transform: 'translateX(0px)', opacity: 0, easing: easeInOutQuint, offset: 0.437 },
    { opacity: 1, offset: 0.457 },
    { transform: 'translateX(6.2px)', opacity: 0, offset: 0.477 },
    // activate
    { transform: 'translateX(6.2px)', opacity: 0, easing: easeInOutQuint, offset: 0.603 },
    { transform: 'translateX(6.2px)', opacity: 0, offset: 1 }
  ],
  slashSlide: [
    // animate
    { transform: 'translateX(0)', offset: 0 },
    { transform: 'translateX(0)', opacity: 1, offset: 0.077, easing: easeInOutQuint },
    { opacity: 0, offset: 0.103 },
    { transform: 'translateX(-75px)', opacity: 1, offset: 0.127 },
    // activate
    { transform: 'translateX(-75px)', opacity: 1, offset: 0.247, easing: easeInOutQuint },
    { opacity: 0, offset: 0.270 },
    { transform: 'translateX(0)', opacity: 1, offset: 0.293 },
    // animate
    { transform: 'translateX(0)', opacity: 1, offset: 0.410, easing: easeInOutQuint },
    { opacity: 0, offset: 0.437 },
    { transform: 'translateX(-75px)', opacity: 1, offset: 0.460 },
    // activate
    { transform: 'translateX(-75px)', opacity: 1, offset: 0.580, easing: easeInOutQuint },
    { transform: 'translateX(-75px)', offset: 1 }
  ],
  slashSlideBlur: [
    // animate
    { transform: 'translateX(0)', opacity: 0, offset: 0 },
    { transform: 'translateX(0)', opacity: 0, offset: 0.077, easing: easeInOutQuint },
    { opacity: 1, offset: 0.103 },
    { transform: 'translateX(-75px)', opacity: 0, offset: 0.127 },
    // activate
    { transform: 'translateX(-75px)', opacity: 0, offset: 0.247, easing: easeInOutQuint },
    { opacity: 1, offset: 0.270 },
    { transform: 'translateX(0)', opacity: 0, offset: 0.293 },
    // animate
    { transform: 'translateX(0)', opacity: 0, offset: 0.410, easing: easeInOutQuint },
    { opacity: 1, offset: 0.437 },
    { transform: 'translateX(-75px)', opacity: 0, offset: 0.460 },
    // activate
    { transform: 'translateX(-75px)', opacity: 0, offset: 0.580, easing: easeInOutQuint },
  ],
  firstWordSlide: [
    // animate
    { transform: 'translateY(0px)', opacity: 1, offset: 0.087, easing: easeInQuint },
    { opacity: 0, offset: 0.113 },
    { transform: 'translateY(12px)', opacity: 0, offset: 0.137 },
    // activate
    { transform: 'translateY(-12px)', opacity: 0, offset: 0.283, easing: easeOutBack },
    { opacity: 0, offset: 0.303 },
    { opacity: 1, offset: 0.320 },
    { transform: 'translateY(0px)', offset: 0.333 },
    // animate
    { transform: 'translateY(0px)', opacity: 1, offset: 0.420, easing: easeInQuint },
    { opacity: 0, offset: 0.447 },
    { transform: 'translateY(12px)', opacity: 0, offset: 0.470 },
    // activate
    { transform: 'translateY(12px)', opacity: 0, offset: 1 },
  ],
  firstWordSlideBlur: [
    // animate
    { opacity: 0, offset: 0 },
    { transform: 'translateY(0px)', opacity: 0, offset: 0.087, easing: easeInQuint },
    { opacity: 1, offset: 0.113 },
    { transform: 'translateY(12px)', opacity: 0, offset: 0.137 },
    // activate
    { transform: 'translateY(-12px)', opacity: 0, offset: 0.283, easing: easeOutBack },
    { opacity: 1, offset: 0.303 },
    { opacity: 0, offset: 0.320 },
    { transform: 'translateY(0px)', opacity: 0, offset: 0.333 },
    // animate
    { transform: 'translateY(0px)', opacity: 0, offset: 0.420, easing: easeInQuint },
    { opacity: 1, offset: 0.447 },
    { transform: 'translateY(12px)', opacity: 0, offset: 0.470 },
    // activate
    { transform: 'translateY(12px)', opacity: 0, offset: 1 },
  ],
  secondWordSlide: [
    // animate
    { transform: 'translateY(-12px)', opacity: 0, offset: 0 },
    { transform: 'translateY(-12px)', offset: 0.117, easing: easeOutBack },
    { opacity: 0, offset: 0.130 },
    { opacity: 1, offset: 0.143 },
    { transform: 'translateY(0px)', opacity: 1, offset: 0.167 },
    // activate
    { transform: 'translateY(0px)', opacity: 1, offset: 0.253, easing: easeInQuint },
    { opacity: 0, offset: 0.280 },
    { transform: 'translateY(12px)', opacity: 0, offset: 0.303 },
    { transform: 'translateY(12px)', opacity: 0, offset: 0.333 },
    // animate
    { transform: 'translateY(-12px)', opacity: 0, offset: 0.340 },
    { transform: 'translateY(-12px)', offset: 0.450, easing: easeOutBack },
    { opacity: 0, offset: 0.463 },
    { opacity: 1, offset: 0.477 },
    { transform: 'translateY(0px)', opacity: 1, offset: 0.500 },
    // activate
    { transform: 'translateY(0px)', opacity: 1, offset: 0.587, easing: easeInQuint },
  ],
  secondWordSlideBlur: [
    // animate
    { transform: 'translateY(-12px)', opacity: 0, offset: 0 },
    { transform: 'translateY(-12px)', opacity: 0, offset: 0.117, easing: easeOutBack },
    { opacity: 1, offset: 0.130 },
    { opacity: 0, offset: 0.143 },
    { transform: 'translateY(0px)', opacity: 0, offset: 0.167 },
    // activate
    { transform: 'translateY(0px)', opacity: 0, offset: 0.253, easing: easeInQuint },
    { opacity: 1, offset: 0.280 },
    { transform: 'translateY(12px)', opacity: 0, offset: 0.303 },
    { transform: 'translateY(12px)', opacity: 0, offset: 0.333 },
    // animate
    { transform: 'translateY(-12px)', opacity: 0, offset: 0.340 },
    { transform: 'translateY(-12px)', opacity: 0, offset: 0.450, easing: easeOutBack },
    { opacity: 1, offset: 0.463 },
    { opacity: 0, offset: 0.477 },
    { transform: 'translateY(0px)', opacity: 0, offset: 0.500 },
    // activate
    { transform: 'translateY(0px)', opacity: 0, offset: 0.587, easing: easeInQuint },
    { opacity: 0, offset: 1 }
  ],
  clipSlide: [
    { transform: 'translateX(0px)', opacity: 1, offset: 0 },
    { transform: 'translateX(0px)', opacity: 1, offset: 0.587, easing: 'cubic-bezier(0.9, 0, 0.25, 1.2)' },
    { opacity: 0.3, offset: 0.622 },
    { opacity: 1, offset: 0.639 },
    { transform: 'translateX(75px)', opacity: 1, offset: 0.656 },
    { transform: 'translateX(75px)', opacity: 1, offset: 1 }
  ],
  clipSlideBlur: [
    { transform: 'translateX(0px)', opacity: 0, offset: 0 },
    { transform: 'translateX(0px)', opacity: 0, offset: 0.587, easing: 'cubic-bezier(0.9, 0, 0.25, 1.2)' },
    { opacity: 1, offset: 0.622 },
    { opacity: 0, offset: 0.639 },
    { transform: 'translateX(75px)', opacity: 0, offset: 0.656 },
    { transform: 'translateX(75px)', opacity: 0, offset: 1 }
  ]
}

// helper function to shift keyframes around:

// function offsetOffset (object, decimal) {
//   const animations = Object.values(object)
//   animations.forEach((animation) => {
//     animation.forEach((keyframe) => {
//       const newOffset = (keyframe.offset * decimal).toFixed(3)
//       keyframe.offset = newOffset
//     })
//   })
//   console.log(object)
// }
//
// offsetOffset(keyframes, 0.6667)

function animateElement (id, keyframeObject, options) {
  const element = document.getElementById(id)
  element.animate(keyframeObject, options)
}

function createAnimations (idArray, keyframesName, options) {
  const baseAnimations = idArray.map((id) => {
    animateElement(id, keyframes[keyframesName], options)
  })
  const blurAnimations = idArray
    .filter(id => document.getElementById(id + '-blur'))
    .map(id => id + '-blur')
    .map((blurId) => {
      animateElement(blurId, keyframes[keyframesName + 'Blur'], options)
    })

  return [...baseAnimations, ...blurAnimations]
}

const animations = [
  ...createAnimations(['n', 'm'], 'firstWordSlide', defaultTimings),
  ...createAnimations(['c', 't', 'v'], 'secondWordSlide', defaultTimings),
  ...createAnimations(['i'], 'iSlide', defaultTimings),
  ...createAnimations(['slash'], 'slashSlide', defaultTimings),
  ...createAnimations(['clippy', 'slash-activate'], 'clipSlide', defaultTimings)
]

const animatedSvg = document.getElementById('animate-slash-activate')

// pause or play on click

animatedSvg.addEventListener('click', pauseOrPlay, false)

function pauseOrPlay (event) {
  document.getAnimations().forEach((animation) => {
    if (event.currentTarget.contains(animation.effect.target)) {
      (animation.playState === 'running') ? animation.pause() : animation.play()
    }
  })
}
